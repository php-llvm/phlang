# Phlang

Welcome to phlang - open source cross-platform PHP compiler based on llvm/clang compiler framework!

## About

This project is an attempt to create PHP compiler (not JIT, not AOT), which would:

  * support latest version of PHP syntax,
  * create executables with low memory consumption,
  * provide exhaustive diagnostics,
  * support seamless interface with C++.

Supported on Linux and Windows.

## Building from source

1. Ensure that you have enough modern compiler:

  * GCC 4.8
  * Visual Studio 2015
  * Clang 3.1

    On Windows you also need to install [GnuWin32 utilities](http://gnuwin32.sourceforge.net/) if you want to run tests.

2. Check out project root repository and submodules:

        git clone https://bitbucket.org/php-llvm/phlang.git
        cd phlang
        git submodule init
        git submodule update

    On Windows you need to use proper git client, or use bash emulator.

3. You can execute script that checks out remaining repositories:

        PHLANGROOT=`pwd`
        ./scripts/clone.sh

    Alternatively you can check out them manually:

        cd phlang
        PHLANGROOT=`pwd`
        cd llvm/tools
        git clone https://bitbucket.org/php-llvm/clang.git
        cd clang
        git clone https://bitbucket.org/php-llvm/phpfe.git
        cd phpfe
        git clone https://bitbucket.org/php-llvm/runtime.git

4. Create build directory. It may be at any place, in this example it placed in the directory *phlang*. Copy file scripts/conf.sh (or scripts/conf.bat on Windows) into the build directory. You may want to change some options in this file. Then run it:

        cd $PHLANGROOT
        mkdir build
        cd build
        cp ../scripts/conf.sh .
        ./conf.sh

    The script must finish successfully.

5. Run make in the build directory.

        make

    You may run parallel build by specifying option -jN, where N is an integer number appropriate for the number of CPU cores and available RAM.

    On Windows build directory must contain LLVM.sln, which can be opened by Visual Studio.

## Rinning compiler

Prepare PHP source file, for instance:

    <?php
    echo "Hello, world!\n";
    ?>

and run compiler on it:

    path/to/compiler/phlang hw.php -o hw

You will get executable file *hw* in the current directory, which is a compiled version of your script. Path to compiler depends on OS and build options, in this examle it must be *phlang/build/bin*.