#!/bin/bash

if [ ! -d llvm/tools ]; then
  echo "Submodules were not initialized properly"
  exit 1
fi

# The first argument, if specified, is BitBucket user name.
BBUSER=
if [ "$#" = "0" ]; then
  :
elif [ "$#" = "1" ]; then
  BBUSER="$1"
else
  echo "Usage: clone.sh <your_bitbucket_username>"
  exit 1
fi

if [ -z "$BBUSER" ]; then
  REPOROOT=https://bitbucket.org/php-llvm
else
  REPOROOT=https://$BBUSER@bitbucket.org/php-llvm
fi

# Update branches of git repositories from the remote server
WSROOT=`pwd`

echo "clang"
cd llvm/tools
if [ -d clang ]; then
  cd clang
  if ! git fetch; then
    echo "Failed to fetch origin from repository clang"
    exit 1
  fi
  if ! git rebase origin/phl; then
    echo "Failed to rebase to phl in repository clang"
    exit 1
  fi
else
  if ! git clone $REPOROOT/clang.git; then
    echo "Cannot clone repository clang"
    exit 1
  fi
  cd clang
  git checkout phl
fi

echo "phpfe"
if [ -d phpfe ]; then
  cd phpfe
  if ! git fetch; then
    echo "Failed to fetch origin from repository phpfe"
    exit 1
  fi
  if ! git rebase origin/master; then
    echo "Failed to rebase to master in repository phpfe"
    exit 1
  fi
else
  if ! git clone $REPOROOT/phpfe.git; then
    echo "Cannot clone repository phpfe"
    exit 1
  fi
  cd phpfe
fi


echo "runtime"
if [ -d runtime ]; then
  cd runtime
  if ! git fetch; then
    echo "Failed to fetch origin from repository runtime"
    exit 1
  fi
  if ! git rebase origin/master; then
    echo "Failed to rebase to master in repository runtime"
    exit 1
  fi
else
  if ! git clone $REPOROOT/runtime.git; then
    echo "Cannot clone repository runtime"
    exit 1
  fi
fi
