PATH=C:\Program Files (x86)\CMake\bin;%PATH%

rem Must contain path to the directory that contain GNU win32 utilities
set TOOLS_DIR=C:\arbeit\gnuwin32\bin

cmake -G "Visual Studio 12" ^
      -DCMAKE_BUILD_TYPE=Debug ^
      -DLLVM_TARGETS_TO_BUILD=X86 ^
      -DLLVM_BUILD_EXAMPLES=OFF ^
      -DLLVM_INCLUDE_EXAMPLES=OFF ^
      -DLLVM_LIT_TOOLS_DIR=%TOOLS_DIR% ^
      -DCLANG_ENABLE_ARCMT=OFF ^
      -DCLANG_ENABLE_STATIC_ANALYZER=OFF ^
      -DCLANG_BUILD_EXAMPLES=OFF ^
      -DCLANG_ENABLE_PHP_FRONTEND=ON ^
      ..\llvm

