cmake -G "Unix Makefiles" \
  -DCMAKE_BUILD_TYPE=Debug \
  -DLLVM_TARGETS_TO_BUILD=X86 \
  -DLLVM_BUILD_EXAMPLES=OFF \
  -DLLVM_INCLUDE_EXAMPLES=OFF \
  -DCLANG_ENABLE_ARCMT=OFF \
  -DCLANG_ENABLE_STATIC_ANALYZER=OFF \
  -DCLANG_BUILD_EXAMPLES=OFF \
  -DCLANG_ENABLE_PHP_FRONTEND=ON \
  ../llvm
