#!/usr/bin/env python

#
#  LLVM based PHP compiler
#
# This file is distributed under the University of Illinois Open Source
# License. See LICENSE.TXT for details.
#
#===----------------------------------------------------------------------===//

# This script is designed to simplfy updating multiiple repositaries used
# in LLVM based PHP compiler project. 

# Script accepts following parameters:
# USERNAME - username for accessing repotaries. It is assumed that the same
# username is used for all repositaries
# ROOT_DIRECTORY - root directory where all repositaries will be places
# defaults to current directory

import sys
import os
import subprocess
import collections
import shutil

from subprocess import call


def print_help():
    print "Usage: update.py <username> [<source_directory> = current directory]"


#username for accessing repositary

if (len(sys.argv) < 2):
    print "Error: no username specified"
    print_help()
    exit(2)

bbuser = sys.argv[1]
rootdir = ""
if (len(sys.argv) > 2):
   rootdir = os.path.abspath(sys.argv[2])
else:
   rootdir = os.path.abspath(os.getcwd())

# This function performs following steps:
# 1) If repositary already exists, execute git fetch
#    otherwise execute git clone 
# 2) If specific revision was provided, check is we are
#    already on this revision. Is so - update ia complete.
# 3) Checkout specified branch
# 4) Rebase to origin/branch
# 5) If specific revision is provided check it out

def update_rep(path, url, branch, name, revision = ""):
    print "\n"
    print "==========================================="
    print "Updating " + name
    print "==========================================="
    print "\n"
    if os.path.isdir(path + "/.git"):
       os.chdir(path)
       call(["git","fetch"])
       # if we are already on correct revision exit
       if revision != "": 
          revision_check = subprocess.Popen(["git","rev-parse","HEAD"],stdout=subprocess.PIPE)
          result = revision_check.communicate()
          if result[0].rstrip() == revision:
              print "Already on correct revision"
              return
          else:
             call(["git", "checkout", revision]) 
       else:
          # otherwise checkout target branch and preform rebase
          call(["git", "checkout", branch])
          call(["git","rebase", "origin/" + branch])       
    else:
       call(["git","clone","https://" + bbuser + "@" + url, path, "-b", branch])
       if revision != "":
           os.chdir(path)
           call(["git", "checkout", revision])   
    print "==========================================="


# Updating llvm
update_rep(rootdir + "/llvm", "github.com/llvm-mirror/llvm.git", "master", "llvm", "573624bfdfc95741e581d630296a91a7d1d039c2")
# Updating clang
update_rep(rootdir + "/llvm/tools/clang", "bitbucket.org/php-llvm/clang.git", "phl", "clang")
# Updating phpfe
update_rep(rootdir + "/llvm/tools/clang/phpfe", "bitbucket.org/php-llvm/phpfe.git", "master", "phpfe")
# Updating runtime
update_rep(rootdir + "/llvm/tools/clang/phpfe/runtime", "bitbucket.org/php-llvm/runtime.git", "master", "runtime")
# Updating phlang
update_rep(rootdir + "/phlang", "bitbucket.org/php-llvm/phlang.git", "master", "phlang")

print "Done"